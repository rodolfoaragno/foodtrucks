from django.shortcuts import render
from django.http import JsonResponse

def index(request):
    return render(request, 'index.html')

def getFood(request):
    data = {
            'FoodType':0
            }
    return JsonResponse(data)
